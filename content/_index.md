---
paige:
  style: |
    #paige-collections,
    #paige-sections,
    #paige-pages {
        display: none;
    }
    #paige-title {
        font-size: 4rem;
    }
# title: "Topics in Adversarial Machine Learning"
---

<font size = "5"> Seminar Course, Summer 2023 

Department of Computer Science, Saarland University

**Instructor:** [Xiao Zhang](https://xiao-zhang.net/), CISPA Helmholtz Center for Information Security 

**Meeting Time & Location:** 14:15 - 15:45 on Wednesdays in Room 0.07, CISPA Main Building

</font>
<br/><br/>

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="images/aml_cartoon.jpeg"
    imageclass="rounded-4 shadow"
    stretch=false >}}
{{< /paige/hero >}}
<center> Image credit: <a href="https://hackernoon.com/adversarial-machine-learning-a-beginners-guide-to-adversarial-attacks-and-defenses"> HackerNoon </a></center>

<br/><br/>

<font size = "5">
In this seminar, we will focus on understanding the security threats adversaries pose to machine learning systems (evasion and poisoning attacks) and the recent algorithmic advancements of building more robust machine learning systems to mitigate those threats. In addition, we will look into several theoretical works on understanding and characterizing the fundamental limits of adversarial machine learning. 
<font size = "5">

<br/><br/>