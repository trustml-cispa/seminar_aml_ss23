---
title: "Teams"
description: "Team assignment and topic assignment will be posted here."
# draft: true
---

Below is the topic assignment for presentations after gathering all the topic preferences. Note that each student is assiged to (at most) two reseach topics.

| Topic                                           | Assigned Students           |
| :----                                           | :----                       |
| Adversarial Examples & Robustness Evaluation    | Shreyash (Guest)            |
| Robustness Certification Methods                | Gopal                       |
| Robust Overfitting & Mitigation Methods         | Xiao                        |
| Robust Generalization & Semi-Supervised Methods | Somrita & Shreyash (Guest)  |
| Indiscriminate Poisoning & Backdoor Attacks     | Baoshuang & Wenhao (Guest)  |	
| Targeted Poisoning Attacks & Certification      | Gopal                       |	
| Intrinsic Limits on Adversarial Robustness      | Somrita                     |
| Adversarial ML Beyond Image Classification      | Baoshuang                   |	
| <img width=500/>                                | <img width=500/>            |

For team project and final seminar paper, the course instructor will not assign you to a team. You can choose your teammate and form the team based on your interests. In principal, the team should consist of 2-3 students, but you are also allowed to write the seminar paper by yourself if you are deteremined to.