---
title: "Blogging Mechanics"
description: "A simple guide on how to create class blog posts"
weight: 20
# draft: true
---

I believe each team has at least a team member with enough experience using git and web construction tools such that following the instructions will not be a big burden. To create a blog post documenting the in-class activities for your assigned class, you can follow the following steps:

- Install [Hugo](https://gohugo.io/installation/), one of the most popular open-source static site generators. For macOS, Hugo can be simply installed using [homebrew](https://brew.sh/): _brew update && brew install hugo_.

- Download the gitlab repository: https://gitlab.com/trustml-cispa/seminar_aml_ss23. This is what is used to build our [course website](https://trustml-cispa.gitlab.io/seminar_aml_ss23/). If you are working with multiple teammates on the blog post (which you probably should be), you can add write permissions for everyone to the cloned repository.

- You should modify the corresponding .md file in the ```/content/post/``` folder. You can start by updating the description in the header and adding your content after the header. You should keep _draft: true_ in the header, so your page will not appear on the course website until it is ready. I will comment out the line _draft: true_ only when your page is ready to be deployed.

- Use the ```/static/images``` folder for images and ```/static/docs``` folder for documents. It is highly encouraged to include images and other visual resources to make your post interesting.

- Write the content of your blog post using Markdown, a lightweight markup language that you can use to add formatting elements to plaintext text documents. You could probably figure out most things by looking at previous posts, but check [this page](https://daringfireball.net/projects/markdown/syntax) for a summary of Markdown syntax. 

- You can incorporate latex math into your markdown. Use ```\\( ... \\)``` for inline math and ```$$ ... $$``` for display math. For example, when \\(a \ne 0\\), there are two solutions to \\(ax^2 + bx + c = 0\\) and they are $$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$

- Your post should include credits for any external material you use, especially for any images that you include but did not produce by yourself. Also, you should include full references to the papers and their links (usually this refers to ArXiv or a conference site). It's recommended to have an overview section at the beginning of your post with links to all the main papers covered, and then to include links in the corresponding sections.

- To test your post locally, run ```hugo server```. This starts the Hugo development server, usually on port 1313 (unless that port is already in use). Then, you can view the site with a browser at ```http://localhost:1313/seminar_aml_ss23/```.

- When you are ready, submit a pull request to incorporate your changes into the main repository (and public course website). Also, send a message to the course instructor (which is me), so I know the post is ready to review. At this stage, I might make some requests for improvements, and then will post the edited version to the course website.

<br/><br/>
