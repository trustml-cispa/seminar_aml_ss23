---
title: "Course Schedule"
description: "A complete course schedule will be posted here after team and topic assignments."
---

According to the results received from the two online polls, the weekly class meeting time is going to be __14:15 - 15:45 on each Wednesday__ of the class meeting week. The detailed presentation schedule with presenter information is summarized below. Note that this schedule might be subject to change.

| Week              | Topic                                           | Presenters                  |
| :----             | :----                                           | :----                       |
| 08.05 - 12.05     | Adversarial Examples & Robustness Evaluation    | Shreyash (Guest)            |
| 15.05 - 19.05     | Robustness Certification Methods                | Gopal                       |
| 22.05 - 26.05     | Robust Overfitting & Mitigation Methods         | Xiao                        |
| 29.05 - 02.06     | Robust Generalization & Semi-Supervised Methods | Somrita & Shreyash (Guest)  |
| 12.06 - 16.06     | Indiscriminate Poisoning & Backdoor Attacks     | Baoshuang & Wenhao (Guest)  |	
| 19.06 - 23.06     | Targeted Poisoning Attacks & Certification      | Gopal                       |	
| 26.06 - 30.06     | Intrinsic Limits on Adversarial Robustness      | Somrita                     |
| 10.07 - 14.07     | Adversarial ML Beyond Image Classification      | Baoshuang                   |	
| <img width=200/>  | <img width=500/>                                | <img width=500/>            |