---
title: "Class 4: Robust Generalization & Semi-Supervised Methods"
description: "A blog post for class 4 (created by Somrita Ghosh on 8 June, 23)."
weight: -40
draft: false
---

Here are the [PPT slides](/docs/slides_topic4_somrita_shreyash.pdf) for the presentation. The slides were made by Somrita and Shreyash.


We first discuss the motivation for studying robust generalization and semi-supervised methods for adversarially robust learning.In particular, the key findings of the paper [Adversarially Robust Generalization Requires More Data](https://proceedings.neurips.cc/paper_files/paper/2018/file/f708f064faaf32a43e4d3c784e6af9ea-Paper.pdf) can be summerized as follows:

  1. Adversarial perturbations can mislead highly accurate classifiers with small changes in inputs.
  2. Robust learning requires significantly more training data compared to standard learning.
  3. Experimental results confirm the existence of a sample complexity gap between robust learning and standard learning.

We can see in the figure below that current approaches may be unable to attain higher adversarial accuracy on datasets such as CIFAR-10 for a fundamental reason: the dataset may not be large enough to train a standard convolutional network robustly. 

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image_1.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

<!-- ![Figure:1](/images/class4/image_1.png) -->

<!-- <img src="/images/class4/image1.png" alt="Figure:1" width="60%" height="60%"> -->
<!-- 
&nbsp;&nbsp;
&nbsp;&nbsp;[Figure:1]
&nbsp;&nbsp; -->

However, addressing the challenge of insufficient amount of training data, particularly with high-quality labels, can be difficult and costly. To enhance robust accuracy and expand the dataset, we discussed the following two papers:
  - [Unlabeled Data Improves Adversarial Robustness](https://arxiv.org/pdf/1905.13736.pdf)
  - [Robust learning meets generative models: Can proxy distributions improve adversarial robustness?](https://arxiv.org/pdf/2104.09425.pdf)

&nbsp;&nbsp;
&nbsp;&nbsp;

### Unlabeled Data Improves Adversarial Robustness

This paper uses a type of semisupervised learning called self-training to improve the robust accuracy of a model using less labeled data and a lot of unlabeled data. The process of self-training typically involves the following steps:
&nbsp;&nbsp;
1. Initial Training: Start by training a model on the available labeled data. This initial model serves as a starting point for the self-training process.

2. Model Prediction: Use the trained model to make predictions on unlabeled data. These predictions are generated based on the model's current understanding of the task.

3. Expanded Dataset: Combine the labeled data from the initial dataset with the newly pseudo-labeled data. This enlarged dataset consists of both the original labeled data and the newly labeled instances.

4. Retraining: Train the model on the expanded dataset, including the pseudo-labeled data. This step updates the model's parameters and improves its performance.

The above process help in improve the classifier's robustness by a significant amount. 

#### The following example and theoretical results from the paper demonstrate the value of unlabeled data:

<!-- &nbsp;&nbsp; -->
{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image2.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

<!-- <img src="image2.png" alt="Figure:2" width="90%" height="60%"> -->
<!-- &nbsp;&nbsp;
&nbsp;&nbsp;[Figure:2]
&nbsp;&nbsp; -->

In particular, the following proposition characterizes the sample complexity for a fully supervised learning method.

__Proposition:__ There exists a universal constant \\( r \\) such that for all \\( \epsilon^2 \sqrt{d/n_0} \geq r \\),

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/proposition_fig.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

Moreover, as the following theorem states, no learning algorithm can produce a classifier with nontrivial robust error without observing \\( \tilde\Omega(n_0 \cdot \epsilon^2 \sqrt{d/n_0}) \\) examples. Thus, a sample complexity gap forms as \\( d \\) grows. Moving to the semisupervised case we can prove the following theorem.

__Theorem:__ There exists a universal constant \\( \tilde{r} \\) such that for all \\( \epsilon^2 \sqrt{d/n_0} \geq r \\), \\( n  \geq n_0 \\) labeled data and additional \\( \tilde{n} \\) unlabeled data,

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/theorem_fig.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

Therefore, compared to the fully supervised case, the self-training classifier requires only a constant factor more input examples, and roughly a factor \\( \epsilon^2 \sqrt{d/n_0} \\) fewer labels.

<!-- &nbsp;&nbsp;
&nbsp;&nbsp; -->

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image3.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

<!-- &nbsp;&nbsp;
&nbsp;&nbsp;[Figure:3]
&nbsp;&nbsp; -->

&nbsp;&nbsp;
The following experiment demonstrates that most of the gain comes from additional inputs rather than additional labels:
<!-- &nbsp;&nbsp; -->

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image4.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

<!-- &nbsp;&nbsp;
&nbsp;&nbsp;[Figure:4]
&nbsp;&nbsp;
&nbsp;&nbsp; -->


&nbsp;&nbsp;
### Robust learning meets generative models: Can proxy distributions improve adversarial robustness?

This paper proposes the idea of leveraging samples from proxy distributions as a means to enhance robustness. They begin by establishing analytical bounds on the transfer of adversarial robustness from proxy data to real-world data distributions. Additionally, they introduce a metric based on these bounds, which effectively assesses the relative ranking of different proxy distributions in terms of robustness transfer. Notably, their metric can be empirically computed using samples from both the proxy and real data distributions, eliminating the need for knowledge about the specific distributions. Lastly, they present their robust training formulation called PORT (Proxy-based Robust Training), which combines synthetic samples generated by a generative model with real data samples to improve robustness. 

#### Answers to questions asked 
  1. Does robustness transfer from training on synthetic data to real world data?       __Answer:__ Analytical bound
  2. If yes, what features enable transfer?         __Answer:__ Experimental proofs
  3. How to optimize selection of generative models?        __Answer:__ Transfer metric
  4. Optimize selection of synthetic samples to maximize robustness transfer?       __Answer:__ Importance metric

#### Understanding Transfer of Adversarial Robustness between Data Distributions

Since the goal is to use samples from proxy distribution to improve robustness on real data distribution, they first study the transfer of adversarial robustness between two data distributions:

<!-- &nbsp;&nbsp; -->

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image5.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

<!-- 
&nbsp;&nbsp;
&nbsp;&nbsp; -->

The goal is to provide a bound on the distribution-shift penalty that is independent of the classifier in hand and is only related to the properties of the distributions. With this goal, they define a notion of distance between two distributions:

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image6.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

#### Which proxy distribution to choose?
1. Generally it is a hard task to empirically calculate the conditional Wasserstein distance using only samples from the distributions (Mahloujifar et al., 2019b; Bhagoji et al., 2019).
2. A metric named ARC as a surrogate for conditional Wasserstein distance that can be calculated when we only have sample access to the data distributions. 

The goal of a robust discriminator \\( \sigma \\) is to maximize its robust accuracy. When selecting a proxy distribution, we choose the one with the smallest ARC value, since it is expected to be closest to real data distribution.

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image7.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

#### Bringing it all together

The objective is to integrate synthetic samples from Proxy distributions in Robust Training (PORT).In particular, they use the following robust training formulation that combines samples from a real training dataset with synthetic samples.

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image8.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}

__Experimental Results:__ The following figure compares experimental results of this framework (PORT) with baselines for adversarial training on the CIFAR-10 dataset for both \\( \ell_\infty \\) and \\( \ell_2  \\) threat model. They sample synthetic images from the diffusion-based generative model (DDPM).

{{< paige/hero
    alt="Landscape"
    format="webp"
    image="/images/class4/image9.png"
    imageclass="rounded-4"
    stretch=false >}}
{{< /paige/hero >}}


